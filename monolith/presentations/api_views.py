from django.http import JsonResponse
from .models import Presentation
from events.models import Conference
from common.json import ModelEncoder
from events.api_views import ConferenceListEncoder
from django.views.decorators.http import require_http_methods
import json, pika


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    fields = [
        "title",
    ]

    def get_extra_data(self, o):
        return {"status": o.status.name}

    encoders = {"conference": ConferenceListEncoder()}


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    fields = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]

    encoders = {"conference": ConferenceListEncoder()}

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentation = [
            {
                "title": p.title,
                "status": p.status.name,
                "href": p.get_api_url(),
            }
            for p in Presentation.objects.filter(conference=conference_id)
        ]

    else:
        presentation = json.loads(request.body)
        conf = presentation["conference"]["name"]
        presentation["conference"] = (Conference.objects.get(name=conf))
        presentation = Presentation.create(**presentation)

    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False
    )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_presentation(request, id):

    if request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": (count > 0)})

    elif request.method == "PUT":
        presentation = json.loads(request.body)
        presentation["conference"] = (
            Conference.objects.get(
                name=(presentation["conference"]["name"])
            )
        )
        Presentation.objects.filter(id=id).update(**presentation)

    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False
    )

@require_http_methods(["PUT"])
def api_approve_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.approve()
    body = {}
    body["presenter_name"] = presentation.presenter_name
    body["presenter_email"] = presentation.presenter_email
    body["title"] = presentation.title
    post_queue(json.dumps(body), "presentation_approvals")
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.reject()
    body = {}
    body["presenter_name"] = presentation.presenter_name
    body["presenter_email"] = presentation.presenter_email
    body["title"] = presentation.title
    post_queue(json.dumps(body), "presentation_rejections")
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )



def post_queue(body, queue_name):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue_name)
    channel.basic_publish(
        exchange="",
        routing_key=queue_name,
        body=body,
    )
    connection.close()

#     presenter_name = models.CharField(max_length=150)
#     company_name = models.CharField(max_length=150, null=True, blank=True)
#     presenter_email = models.EmailField()

#     title = models.CharField(max_length=200)
