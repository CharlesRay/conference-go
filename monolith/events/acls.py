from .keys import PEXEL_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_pic(*args):

    params = {"query": f"{args}"}
    url = "https://api.pexels.com/v1/search"
    info = requests.get(
        url,
        params=params,
        headers={"Authorization": PEXEL_API_KEY}
    ).json()

    picturl = info["photos"][0]["src"]["original"]
    return {"picture": picturl}


def ollie(city, state):
    url = f"https://api.openweathermap.org/geo/1.0/direct?q={city}&limit=10&appid={OPEN_WEATHER_API_KEY}"
    geoinfo = requests.get(url).json()
    lat = ""
    lon = ""
    for result in geoinfo:
        if result["state"] == state:
            lat = result["lat"]
            lon = result["lon"]
            break
    
    weather = requests.get(f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial").json()
    return {
        "description": weather["weather"][0]["description"],
        "temp": weather["main"]["temp"]
    }
