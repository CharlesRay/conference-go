from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .acls import ollie, get_pic
import json


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    fields = ["name"]


class LocationListEncoder(ModelEncoder):
    model = Location
    fields = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    fields = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location"
        ]
    encoders = {
        "location": LocationListEncoder(),
    }


class LocationDetailEncoder(ModelEncoder):
    model = Location
    fields = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conference = Conference.objects.all()

    else:
        content = json.loads(request.body)
        try:
            content["location"] = Location.objects.get(id=content["location"])
            conference = Conference.objects.create(**content)

        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"}, status=400
                )

    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False
    )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, id):
    if request.method == "DELETE":
        conf, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"Delete": conf > 0})

    elif request.method == "PUT":
        conference = json.loads(request.body)
        try:
            conference["location"] = Location.objects.get(
                name=conference["location"]["name"]
            )
            Conference.objects.filter(id=id).update(**conference)

        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400)

    conf = Conference.objects.get(id=id)
    try:
        weather = ollie(conf.location.city, conf.location.state.name)

    except (KeyError,  IndexError, ValueError):
        weather = "Information unavailable"

    return JsonResponse(
        {"weather": weather, "conference": conf},
        encoder=ConferenceDetailEncoder,
        safe=False,
    )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = [
            {"name": loc.name, "href": loc.get_api_url()}
            for loc in Location.objects.all()
            ]
        return JsonResponse({"locations": locations})
    else:
        location = json.loads(request.body)
        location["state"] = State.objects.get(
            abbreviation=location["state"].upper()
        )
        location.update(
            get_pic(location["state"].name, location["city"])
        )
        location = Location.objects.create(**location)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_location(request, id):

    if request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": (count > 0)})

    elif request.method == "PUT":
        location = json.loads(request.body)

        try:
            location["state"] = State.objects.get(
                abbreviation=location["state"].upper()
            )
            Location.objects.filter(id=id).update(**location)

        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400)

    location = Location.objects.get(id=id)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
        )
