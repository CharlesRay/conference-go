from datetime import datetime
import json, pika, os, sys, time, django
from pika.exceptions import AMQPConnectionError


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


def update_accountVO(ch, method, properties, body):
   content = json.loads(body)
   first_name = content["first_name"]
   last_name = content["last_name"]
   email = content["email"]
   is_active = content["is_active"]
   updated_string = content["updated"]
   content["updated"] = datetime.fromisoformat(updated_string)
   if is_active:
        AccountVO.objects.filter(email=email).update_or_create(**content)

   else:
        AccountVO.objects.delete


while True:
   try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.exchange_declare(exchange='account_info', exchange_type='fanout')
        result = channel.queue_declare(queue='', exclusive=True)
        queue_name = result.method.queue
        channel.queue_bind(
            exchange='account_info',
            queue=queue_name
        )
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=update_accountVO,
            auto_ack=True,
        )
        channel.start_consuming()
   except AMQPConnectionError:
        print ("could not connect to RabbitMQ")
        time.sleep(2.0)
