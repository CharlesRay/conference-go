from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee, ConferenceVO, AccountVO
from django.views.decorators.http import require_http_methods
import json


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    fields = [
        "name",
        "email",
        ]


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    fields = ["name", "import_href"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    fields = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]

    encoders = {
        "conference": ConferenceVODetailEncoder()
    }

    def get_extra_data(self, o):
        num = len(AccountVO.objects.filter(email=o.email))
        if num > 0:
            return {"has_account": True}
        else:
            return {"has_account": False}

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendee = Attendee.objects.filter(conference=conference_id)
    else:
        content = json.loads(request.body)
        # try:
        href = f'/api/conferences/{conference_id}/'
        content["conference"] = ConferenceVO.objects.get(import_href=href)
        attendee = Attendee.objects.create(**content)
        # except ConferenceVO.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid conference id"}, status=400)

    return JsonResponse(attendee, encoder=AttendeeListEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):

    if request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": (count > 0)})

    elif request.method == "PUT":
        attendee = json.loads(request.body)
        attendee["conference"] = (
            ConferenceVO.objects.get(
                name=(attendee["conference"]["name"])
            )
        )
        Attendee.objects.filter(id=id).update(**attendee)

    attendee = Attendee.objects.get(id=id)
    return JsonResponse(
        attendee, encoder=AttendeeDetailEncoder, safe=False
    )
