import json, time, pika, django, os, sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    body = json.loads(body)
    name = body["presenter_name"]
    email = body["presenter_email"]
    title = body["title"]
    send_mail(
        "Your presentation has been accepted",
        f"{name}, we're happy to tell you that your presentation {title} has been accepted",
        "admin@conference.go", [email]
    )


def process_rejection(ch, method, properties, body):
    body = json.loads(body)
    name = body["presenter_name"]
    email = body["presenter_email"]
    title = body["title"]
    send_mail(
        "Your presentation has been rejected",
        f"{name}, we're sorry to tell you that your presentation {title} has been rejected",
        "admin@conference.go", [email]
    )


parameters = pika.ConnectionParameters(host='rabbitmq')
while True:
    try:
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        break

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)

channel.queue_declare(queue="presentation_approvals")
channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejection,
    auto_ack=True,
)
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)
channel.start_consuming()
